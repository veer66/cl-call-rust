(ql:quickload 'cffi)

(in-package cffi)

(pushnew #P"./target/release/"
 	 *foreign-library-directories*
 	 :test #'equal)

(load-foreign-library '(:default "libclcallrust"))

(defcfun "cl_add" :int (value :int))

(print (cl-add 20))

