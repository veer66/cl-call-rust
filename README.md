# cl-call-rust

cl-call-rust is for showing how to call Rust function from Common Lisp.

## Prerequisite

1. rustc
2. cargo
3. sbcl
4. quicklisp

## Usage

````
cargo build --release
sbcl --load call-rust.lisp --quit
````

